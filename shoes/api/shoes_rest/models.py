from django.db import models


class BinVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    name = models.CharField(max_length=200)
class Shoe(models.Model):
    manufacturer = models.CharField(max_length=255)
    model_name = models.CharField(max_length=255)
    color = models.CharField(max_length=50)
    picture_url = models.URLField()
    bin_location = models.CharField(max_length=50)

    bin = models.ForeignKey(
        BinVO,
        related_name="Shoe",
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return f"{self.manufacturer} - {self.model_name}"
