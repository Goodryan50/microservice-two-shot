import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';

import HatsList from './HatList';
import HatsForm from './HatForm';

import ShoeList from './ShoeList';
import ShoeForm from './ShoeForm';


function App(props) {
  return (
    <BrowserRouter>
      <Nav />
        <Routes>
          <Route index element={<MainPage />} />

          <Route path="/hats" element={<HatsList getHats={props.hats} />} />
          <Route path="/hats/news" element={<HatsForm getHats={props.hats} />} />

          <Route path="shoes">
            <Route index element={<ShoeList />}/>
            <Route path="new" element={<ShoeForm/>}/>
          </Route>
        </Routes>
    </BrowserRouter>
  );
}


export default App;

// It display a list of hats on your website using, it sets up a "map" inside the website. 
// When clicks on the link or button for "hats," the code tells the computer to show the page with a list of all the hats
