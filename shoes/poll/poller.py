import django
import os
import sys
import time
import json
import requests



sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "shoes_project.settings")
django.setup()

from shoes_rest.models import BinVO

def getShoes():
    response = requests.get("http://wardrobe-api:8000/api/bins/")
    content = json.loads(response.content)
    print(BinVO.objects.all())
    for bin in content["bins"]:
        BinVO.objects.update_or_create(
            import_href=bin["href"],
            name=bin["closet_name"]
        )
    
    
def poll():
    while True:
        print('Shoes poller polling for data')
        try:
            getShoes()
            pass
        except Exception as e:
            print(e, file=sys.stderr)
        time.sleep(5)


if __name__ == "__main__":
    poll()
