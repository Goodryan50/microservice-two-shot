import React, { useState, useEffect } from 'react'; 
import { Link } from "react-router-dom";

const deleteHat = async(href) => {
    const fetchConfig = {
        method: "DELETE",
        "Content-Type" : "application/json",
    }
    await fetch(`http://localhost:8090/${href}`, fetchConfig);
    window.location.reload()
}

function HatsList({}){
    const [hats, setHats] = useState([])
    
    useEffect(() => {
        fetchData();
    },[]);

     const fetchData = async () => {
        const hatUrl="http://localhost:8090/api/hats/"
        

        const hatResponse = await fetch(hatUrl);
        if (hatResponse.ok) {
            const data=await hatResponse.json()
            setHats(data.hats)
        }
     }

    const handleDelete=async(hat) =>{

    };


    return (
      <div>
          <table className="table table-striped">
              <thead>
                  <tr>
                      <th>Style Name</th>
                      <th>Color</th>
                      <th>Fabric</th>
                      <th>Picture</th>
                      <th>Location</th>
                      <th>Delete</th>
                  </tr>
              </thead>
              <tbody>
                  {hats.map(hat => (
                      <tr key={hat.id}>
                          <td>{hat.style_name}</td>
                          <td>{hat.color}</td>
                          <td>{hat.fabric}</td>
                          <td>
                              <img src={hat.picture_url} alt="" width="100px" height="100px" />
                          </td>
                          <td>{hat.location}</td>
                          <td>
                              <button type="button" onClick={() => deleteHat(`api/hats/${hat.id}`)} className="btn btn-danger">
                                  Delete
                              </button>
                          </td>
                      </tr>
                  ))}
              </tbody>
          </table>
          <Link to="news/" className="btn btn-danger btn-sm m-20 w-40">
              Add a new Hat here!
          </Link>
      </div>
  );
}

export default HatsList;

// This code is for a part of a website that shows a list of hats, 
// including details like their style, color, and fabric, and it even shows pictures of the hats. 
// There's a button to add new hats, and next to each hat in the list, there's a "Delete" button that lets you remove a hat from the list. When you click "Delete," the hat gets removed from the website, and the list updates to show only the remaining hats.