import { useEffect, useState } from "react";
import { Link } from "react-router-dom";
const deleteShoe = async (href) => {
    const fetchConfig = {
        method: "DELETE",
        "Content-Type": "application/json",
    }
    await fetch(`http://localhost:8080${href}`, fetchConfig);
    window.location.reload()
}
function ShoesColumn(props) {
    return (
        <div className="col">
            {props.allShoes.map((data) => {
                let shoe = data.shoe[0];
                return (
                    <div key={shoe.href} className="card mb-4" style={{ width: '18rem' }}>
                        <img src={shoe.picture_url} className="card-img-top" alt="..." />
                        <div className="card-body">
                            <h5 className="card-title">{shoe.name}</h5>
                            <h6 className="card-subtitle">{shoe.bin.closet_name}</h6>
                            <button className="btn btn-danger" onClick={() => deleteShoe(shoe.href)}>Delete</button>
                        </div>
                    </div>
                );
            })}
        </div>
    )
}
function ShoesList() {
    const [shoeColumns, setShoeColumns] = useState([])
    const getShoes = async () => {
        try {
            const response = await fetch("http://localhost:8080/api/shoes/")
            if (response.ok) {
                const data = await response.json()

                const requests = [];
                for (let shoe of data.shoes) {
                    const detailUrl = `http://localhost:8080${shoe.href}`;
                    requests.push(fetch(detailUrl));
                }
                const responses = await Promise.all(requests)
                const shoeColumns = [[], [], []];

                let i = 0;
                for (const shoeResponse of responses) {
                    if (shoeResponse.ok) {
                        const details = await shoeResponse.json();
                        shoeColumns[i].push(details);
                        i++;
                        if (i > 2) {
                            i = 0;
                        }
                    } else {
                        console.error(shoeResponse)
                    }
                }
                setShoeColumns(shoeColumns)
            }
        } catch (e) {
            console.error(e)
        }
    }
    useEffect(() => {
        getShoes()
    })
    return (
        <div className="container">
            <h2>Shoes</h2>
            <Link className="btn btn-success m-2 w-100" to="new/">Add Shoe</Link>
            <div className="row">
                {shoeColumns.map((shoeList, index) => {
                    return (
                        <ShoesColumn key={index} allShoes={shoeList} />
                    );
                })}
            </div>
        </div>
    )
}
export default ShoesList;
