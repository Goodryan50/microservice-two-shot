from django.urls import path
from .api_views import api_list_shoe, api_show_shoe

urlpatterns = [
    path('shoes/', api_list_shoe, name='api_list_shoes'),
    #path('create/', create_shoe, name='create_shoe'),
    path('shoes/<int:shoe_id>/', api_show_shoe, name='delete_shoe'),
]