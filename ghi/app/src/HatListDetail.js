import React from 'react';
import { useParams } from "react-router-dom";


function getHat(id, hats) {
 return hats.find(hat => hat.id === id);
}

function HatListDetail( {hats} ) {
    const params = useParams();
    const hat = getHat(Number(params.id), hats)

    return(
        <p>{hat.hat_name}</p>
    )

}


export default HatListDetail;

// The HatListDetail code is a React component that displays details of a specific hat from an array. 
// It uses the useParams hook from react-router-dom to retrieve the hat's ID from the URL parameters. 
// The component then finds and displays the hat's name by matching this ID with the ones in the provided hats array.